# ModularTable
Proyecto realizado para tener un componente Angular que represente un una tabla totalmente modularizable por componentes que se puedan customizar a gusto.

## ¿Como probar el proyecto?
1. Clonar el repositorio.
2. Ejecutar `npm install`
3. Ejecutar `ng serve`

## ¿Ejemplos de implementación?
Verificar la carpeta que se encuentra dentro del directorio `app/test`.

## ¿Qué tengo que hacer para utilizarlo en mi proyecto?
1. Guardar en nuestro proyecto todo lo que esta en la carpeta `lib`.
6. Importar a nuestro `AppModule` el `ModularTableModule`.
