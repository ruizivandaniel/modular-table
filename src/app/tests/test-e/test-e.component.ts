import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'test-e',
  templateUrl: 'test-e.component.html'
})
export class TestEComponent implements OnInit {
  showAsyncRows = false;
  showAsyncAction = false;

  ngOnInit() {
    setTimeout(() => { this.showAsyncRows = true; }, 4000);
    setTimeout(() => { this.showAsyncAction = true; }, 9000);
  }


  asyncAction = (ctx: any) => {
    console.log('action any: ', ctx);
  }
}