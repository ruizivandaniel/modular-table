import { Component } from '@angular/core';

@Component({
  selector: 'test-b',
  templateUrl: 'test-b.component.html'
})
export class TestBComponent {
  doWithoutCtx = () => {
    console.log('Estoy ejecutando doWithoutCtx');
  }
  actionRow = (ctx: any) => {
    console.log(`Estoy ejecutando 'actionRow' con el contexto: ${ctx}`);
  }
}
