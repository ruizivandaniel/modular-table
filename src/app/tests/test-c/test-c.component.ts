import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'test-c',
  templateUrl: 'test-c.component.html'
})
export class TestCComponent implements OnInit {
  myArray = [1,2,3,4,5,6,7,8];
  ngOnInit() {
    setTimeout(() => {
      this.myArray = [8,7,4,6,5,8,9,1,1,1,1,1];
    }, 5000);
  }
}