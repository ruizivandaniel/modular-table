import { Component, OnInit } from '@angular/core';
import { timer } from 'rxjs';


/** Interfaz de ejemplo para el row */
export interface RowAction {
  name: string;
  action: (context: any) => void
};
export interface Row {
  cells: Cell[];
  actions: RowAction[];
}
export interface Cell {
  title: string;
}

/**
 * Utilizo la tabla modular de manera basica.
 */
@Component({
  selector: 'test',
  templateUrl: 'test-d.component.html'
})
export class TestDComponent implements OnInit {

  showAsyncError: boolean = false;

  // rows
  actions = [
    { name: 'action1', action: (ctx: any) => { console.log(`Funcion "action1, contexto: ${ctx}`); } },
    { name: 'action2', action: (ctx: any) => { console.log(`Funcion "action2, contexto: ${ctx}`); } },
  ];

  rows: Row[] = [
    { cells: [{ title: 'cell1' }, { title: 'cell2' }, { title: 'cell3' }], actions: this.actions},
    { cells: [{ title: 'cell1' }, { title: 'cell2' }, { title: 'cell3' }], actions: this.actions},
    { cells: [{ title: 'cell1' }, { title: 'cell2' }, { title: 'cell3' }], actions: this.actions},
    { cells: [{ title: 'cell1' }, { title: 'cell2' }, { title: 'cell3' }], actions: this.actions},
  ];

  ngOnInit(): void {
    timer(2000).subscribe(() => {
      this.rows = [];
    });
    timer(8000).subscribe(() => {
      this.showAsyncError = true;
    });
  }

}
