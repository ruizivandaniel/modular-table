import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { TestAComponent } from './tests/test-a/test-a.component';
import { TestBComponent } from './tests/test-b/test-b.component';
import { TestCComponent } from './tests/test-c/test-c.component';
import { TestDComponent } from './tests/test-d/test-d.component';
import { TestEComponent } from './tests/test-e/test-e.component';
import { TestFComponent } from './tests/test-f/test-f.component';
import { TestGComponent } from './tests/test-g/test-g.component';
import { ModularTableModule } from './lib/modular-table/modular-table.module';
import { CommonModule } from '@angular/common';

import { RouterModule, Routes } from '@angular/router';

export const routes: Routes = [
  { path: '', redirectTo: 'testa', pathMatch: 'full'},
  { path: 'testa', component: TestAComponent },
  { path: 'testb', component: TestBComponent },
  { path: 'testc', component: TestCComponent },
  { path: 'testd', component: TestDComponent },
  { path: 'teste', component: TestEComponent },
  { path: 'testf', component: TestFComponent },
  { path: 'testg', component: TestGComponent },
]

@NgModule({
  declarations: [
    AppComponent,
    TestAComponent,
    TestBComponent,
    TestCComponent,
    TestDComponent,
    TestEComponent,
    TestFComponent,
    TestGComponent,
  ],
  imports: [
    CommonModule,
    BrowserModule,
    ModularTableModule,
    RouterModule.forRoot(routes),
  ],
  bootstrap: [
    AppComponent,
  ]
})
export class AppModule { }
