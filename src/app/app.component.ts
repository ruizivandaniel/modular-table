import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <div class="w-100 h-100">
      <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <div class="container-fluid">
          <div class="navbar-nav">
            <a class="nav-link active" aria-current="page" href="javascript:;">MODULAR TABLE</a>
            <a class="nav-link" href="javascript:;" [routerLink]="['testa']">Test A</a>
            <a class="nav-link" href="javascript:;" [routerLink]="['testb']">Test B</a>
            <a class="nav-link" href="javascript:;" [routerLink]="['testc']">Test C</a>
            <a class="nav-link" href="javascript:;" [routerLink]="['testd']">Test D</a>
            <a class="nav-link" href="javascript:;" [routerLink]="['teste']">Test E</a>
            <a class="nav-link" href="javascript:;" [routerLink]="['testf']">Test F</a>
            <a class="nav-link" href="javascript:;" [routerLink]="['testg']">Test G</a>
          </div>
        </div>
      </nav>
      <div class="container p-0 my-2 border rounded">
        <router-outlet></router-outlet>
      </div>
    </div>
  `
})
export class AppComponent { }
