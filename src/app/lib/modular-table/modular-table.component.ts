import {
  AfterViewInit, Component, ContentChild, ContentChildren, Input, OnDestroy, QueryList
} from '@angular/core';
import { Subject, timer } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { MteRowsGroupComponent } from './components/rows-group/rows-group.component';
import { MteRowComponent } from './components/row/row.component';
import { MteRowDefaultComponent } from './components/row/row-default.component';
import { MteRowErrorComponent } from './components/row/row-error.component';
import { MteRowHeaderComponent } from './components/row/row-header.component';
import { HeaderActionComponent } from './components/header-action/header-action.component';
import { FooterActionComponent } from './components/footer-action/footer-action.component';

@Component({
  selector: 'mte-modular-table',
  templateUrl: 'modular-table.component.html'
})
export class MteModularTableComponent implements AfterViewInit, OnDestroy {

  @Input() showError: boolean = false;
  @Input() autoContractRows: boolean = true;
  @Input() title!: string;
  @Input() subTitle!: string;

  @ContentChildren(MteRowComponent, { descendants: false })
  private _queryRows!: QueryList<MteRowComponent>;
  @ContentChildren(MteRowsGroupComponent, { descendants: false })
  private _queryGroups!: QueryList<MteRowsGroupComponent>;
  @ContentChildren(HeaderActionComponent, { descendants: false })
  private _queryHeaderActions!: QueryList<HeaderActionComponent>;
  @ContentChildren(FooterActionComponent, { descendants: false })
  private _queryFooterActions!: QueryList<FooterActionComponent>;
  
  showGroup!: string; 
  defaultRow!: MteRowDefaultComponent | undefined;
  errorRow!: MteRowErrorComponent | undefined;
  headerRow!: MteRowHeaderComponent | undefined;
  _rows: MteRowComponent[] = [];
  _headerActions: HeaderActionComponent[] = [];
  _footerActions: FooterActionComponent[] = [];
  _desubscribe: Subject<void> = new Subject<void>();

  get headerActions(): HeaderActionComponent[] {
    return this._headerActions;
  }

  get footerActions(): FooterActionComponent[] {
    return this._footerActions;
  }

  get rows(): MteRowComponent[] {
    return this._rows;
  }

  get groups(): MteRowsGroupComponent[] {
    return this._queryGroups.toArray();
  }

  ngAfterViewInit(): void {
    // Se agrego el timer porque es la unica manera de que se muestre el contenido de los child.
    timer(1).pipe(takeUntil(this._desubscribe)).subscribe(() => {
      this._setRows(this._queryRows.toArray());
      this._headerActions = this._queryHeaderActions.toArray();
      this._footerActions = this._queryFooterActions.toArray();
      // Me subscribo al evento de expansion de las rows para luego
      // cerrarlas si esta en true 'autoContractRows'
      if (this.autoContractRows) {
        this._setTriggerRowExpanded();
      }
      this._queryRows.changes.pipe(takeUntil(this._desubscribe))
        .subscribe((changes: QueryList<MteRowComponent>) => {
          this._setRows(changes.toArray());  
        });
      this._queryHeaderActions.changes.pipe(takeUntil(this._desubscribe))
        .subscribe((changes: QueryList<HeaderActionComponent>) => {
          this._headerActions = changes.toArray();  
        });
      this._queryFooterActions.changes.pipe(takeUntil(this._desubscribe))
        .subscribe((changes: QueryList<FooterActionComponent>) => {
          this._footerActions = changes.toArray();
        });
      this._queryGroups.changes.pipe(takeUntil(this._desubscribe))
        .subscribe((changes: QueryList<MteRowsGroupComponent>) => {
          this._queryGroups = changes;
        });
      if (this.groups.length) {
        this.showGroup = this.groups[0].label.id;
      }
    });
  }

  ngOnDestroy(): void {
    this._desubscribe.next();
    this._desubscribe.complete();
  }

  private _setRows(newRows: MteRowComponent[]): void {
    const aux: MteRowComponent[] = [];
    newRows.forEach((row: MteRowComponent) => {
      if (row instanceof MteRowDefaultComponent) {
        this.defaultRow = row;
        return;
      }
      if (row instanceof MteRowErrorComponent) {
        this.errorRow = row;
        return;
      }
      if (row instanceof MteRowHeaderComponent) {
        this.headerRow = row;
        return;
      }
      aux.push(row);
    });
    this._rows = aux;
  }

  private _setTriggerRowExpanded(): void {
    this.rows.forEach((r: MteRowComponent) => {
      r.expanded.subscribe(() => {
        this._contractAllRows(r);
      });
    });
  }

  private _contractAllRows(row: MteRowComponent): void {
    this.rows.forEach((r: MteRowComponent) => {
      if (r !== row) {
        r.contract();
      }
    });
  }

}
