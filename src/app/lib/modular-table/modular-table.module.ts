import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MteModularTableComponent } from './modular-table.component';
import { MteRowsGroupComponent } from './components/rows-group/rows-group.component';
import { MteRowsGroupLabelComponent } from './components/rows-group/components/row-label.component';
import { MteRowComponent } from './components/row/row.component';
import { MteRowDefaultComponent } from './components/row/row-default.component';
import { MteRowErrorComponent } from './components/row/row-error.component';
import { MteRowHeaderComponent } from './components/row/row-header.component';
import { HeaderActionComponent } from './components/header-action/header-action.component';
import { FooterActionComponent } from './components/footer-action/footer-action.component';
import { MteRowCellComponent } from './components/row/components/row-cell.component';
import { MteRowActionComponent } from './components/row/components/row-action.component';
import { MteRowExpandComponent } from './components/row/components/row-expand.component';

@NgModule({
  imports: [
    CommonModule,
  ],
  declarations: [
    MteModularTableComponent,
    MteRowsGroupComponent,
    MteRowsGroupLabelComponent,
    MteRowComponent,
    MteRowCellComponent,
    MteRowActionComponent,
    MteRowDefaultComponent,
    MteRowErrorComponent,
    MteRowHeaderComponent,
    MteRowExpandComponent,
    HeaderActionComponent,
    FooterActionComponent,
  ],
  exports: [
    MteModularTableComponent,
    MteRowsGroupComponent,
    MteRowsGroupLabelComponent,
    MteRowComponent,
    MteRowCellComponent,
    MteRowActionComponent,
    MteRowDefaultComponent,
    MteRowErrorComponent,
    MteRowHeaderComponent,
    MteRowExpandComponent,
    HeaderActionComponent,
    FooterActionComponent,
  ],
})
export class ModularTableModule { }
