import { Component, Input, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'mte-footer-action',
  template: `
    <ng-template #template>
      <ng-content></ng-content>
    </ng-template>
  `
})
export class FooterActionComponent {
  @Input() action!: () => void;

  @ViewChild('template')
  private _template!: TemplateRef<any>;

  get template(): TemplateRef<any> {
    return this._template;
  }

}
