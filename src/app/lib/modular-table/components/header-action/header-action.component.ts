import { Component, Input, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'mte-header-action',
  template: `
    <ng-template #template>
      <ng-content></ng-content>
    </ng-template>
  `
})
export class HeaderActionComponent {
  @Input() action!: () => void;

  @ViewChild('template')
  private _template!: TemplateRef<any>;

  get template(): TemplateRef<any> {
    return this._template;
  }

}
