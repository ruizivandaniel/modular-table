import { Component, Input, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'mte-rows-group-label',
  template: `
    <ng-template #template>
      <ng-content></ng-content>
    </ng-template>
  `
})
export class MteRowsGroupLabelComponent {

  @Input() id!: string;
  
  @ViewChild('template', { static: false })
  private _template!: TemplateRef<any>;

  get template(): TemplateRef<any> {
    return this._template;
  }

}
