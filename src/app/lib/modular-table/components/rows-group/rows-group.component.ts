import {
  AfterContentInit,
  Component, ContentChild, ContentChildren, Input, QueryList, TemplateRef, ViewChild
} from '@angular/core';
import { MteRowDefaultComponent } from '../row/row-default.component';
import { MteRowErrorComponent } from '../row/row-error.component';
import { MteRowComponent } from '../row/row.component';
import { MteRowsGroupLabelComponent } from './components/row-label.component';

@Component({
  selector: 'mte-rows-group',
  templateUrl: 'rows-group.component.html'
})
export class MteRowsGroupComponent implements AfterContentInit {

  @Input() showError: boolean = false;

  @ViewChild('template', { static: false }) private _template!: TemplateRef<any>;
  @ContentChildren(MteRowComponent, { descendants: false })
  private _queryRows!: QueryList<MteRowComponent>;
  @ContentChild(MteRowsGroupLabelComponent, { static: false })
  private _label!: MteRowsGroupLabelComponent;

  private _rows: MteRowComponent[] = [];
  defaultRow!: MteRowDefaultComponent | undefined;
  errorRow!: MteRowErrorComponent | undefined;

  get label(): MteRowsGroupLabelComponent {
    return this._label;
  }

  get rows(): MteRowComponent[] {
    return this._rows;
  }

  get template(): TemplateRef<any> {
    return this._template;
  }

  ngAfterContentInit(): void {
    this._setRows(this._queryRows.toArray());
    this._queryRows.changes
      .subscribe((changes: QueryList<MteRowComponent>) => {
        this._setRows(changes.toArray());
      });
  }

  private _setRows(newRows: MteRowComponent[]): void {
    newRows.forEach((row: MteRowComponent) => {
      if (row instanceof MteRowDefaultComponent) {
        this.defaultRow = row;
        return;
      }
      if (row instanceof MteRowErrorComponent) {
        this.errorRow = row;
        return;
      }
      this._rows.push(row);
    });
  }

}
