import { Component } from '@angular/core';
import { MteRowComponent } from './row.component';

@Component({
  selector: 'mte-row-on-error',
  template: `
    <ng-template #template>
      <ng-content></ng-content>
    </ng-template>
  `,
  providers: [
    {
      provide: MteRowComponent,
      useExisting: MteRowErrorComponent,
    }
  ]
})
export class MteRowErrorComponent extends MteRowComponent {}
