import {
  AfterContentInit, Component, ContentChild, ContentChildren, EventEmitter, Input, Output, QueryList, TemplateRef,
  ViewChild
} from '@angular/core';
import { MteRowCellComponent } from './components/row-cell.component';
import { MteRowActionComponent } from './components/row-action.component';
import { MteRowExpandComponent } from './components/row-expand.component';

@Component({
  selector: 'mte-row',
  templateUrl: 'row.component.html'
})
export class MteRowComponent implements AfterContentInit {

  @Input() context!: any;
  @Input() justifyActions: 'start' | 'center' | 'end' = 'center';
  @Input() showBackground: boolean = true;
  @Output() expanded: EventEmitter<void> = new EventEmitter<void>();
  
  @ViewChild('template')
  private _template!: TemplateRef<any>;
  @ContentChildren(MteRowCellComponent, { descendants: false })
  private _queryCells!: QueryList<MteRowCellComponent>;
  @ContentChildren(MteRowActionComponent, { descendants: false })
  private _queryActions!: QueryList<MteRowActionComponent>;
  @ContentChild(MteRowExpandComponent, { static: false })
  private _expandedTemplate!: MteRowExpandComponent;

  onOver: boolean = false;
  mustExpand: boolean = false;
  
  get template(): TemplateRef<any> {
    return this._template;
  }

  get expandedTemplate(): MteRowExpandComponent {
    return this._expandedTemplate;
  }
  
  get cells(): MteRowCellComponent[] {
    return this._queryCells.toArray();
  }

  get actions(): MteRowActionComponent[] {
    return this._queryActions.toArray();
  }
  
  ngAfterContentInit(): void {
    this._queryCells.changes
      .subscribe((changes: QueryList<MteRowCellComponent>) => {
        this._queryCells = changes;
      });
    this._queryActions.changes
      .subscribe((changes: QueryList<MteRowActionComponent>) => {
        this._queryActions = changes;
      });
  }

  execRowAction(func: (context: any) => void): void {
    if (func) {
      func(this.context);
    }
  }

  toggle(): void {
    if (this.expandedTemplate) {
      this.mustExpand ? this.contract() : this.expand();
    }
  }

  expand(): void {
    if (!this.mustExpand && this.expandedTemplate) {
      this.mustExpand = true;
      this.expanded.emit();
    }
  }

  contract(): void {
    if (this.mustExpand) {
      this.mustExpand = false;
    }
  }


}
