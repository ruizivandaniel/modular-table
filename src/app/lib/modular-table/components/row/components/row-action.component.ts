import { Component, Input, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'mte-row-action',
  template: `
    <ng-template #template>
      <ng-content></ng-content>
    </ng-template>
  `
})
export class MteRowActionComponent {

  /** recibe una funcion que contiene el contexto de la celda actual. */
  @Input() action!: (context: any) => void;

  @ViewChild('template', { static: false })
  private _template!: TemplateRef<any>;

  get template(): TemplateRef<any> {
    return this._template;
  }

}
