import { Component, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'mte-row-expand',
  template: `
    <ng-template #template>
      <ng-content></ng-content>
    </ng-template>
  `
})
export class MteRowExpandComponent {
  @ViewChild('template', { static: false })
  private _template!: TemplateRef<any>;

  get template(): TemplateRef<any> {
    return this._template;
  }

}
