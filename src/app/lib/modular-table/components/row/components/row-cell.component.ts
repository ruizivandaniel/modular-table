import { Component, TemplateRef, ViewChild } from '@angular/core';

@Component({
  selector: 'mte-row-cell',
  template: `
    <ng-template #template>
      <ng-content></ng-content>
    </ng-template>
  `
})
export class MteRowCellComponent {
  @ViewChild('template', { static: false })
  private _template!: TemplateRef<any>;

  get template(): TemplateRef<any> {
    return this._template;
  }

}
