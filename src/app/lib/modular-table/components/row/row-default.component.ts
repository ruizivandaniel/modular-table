import { Component } from '@angular/core';
import { MteRowComponent } from './row.component';

@Component({
  selector: 'mte-row-default',
  template: `
    <ng-template #template>
      <ng-content></ng-content>
    </ng-template>
  `,
  providers: [
    {
      provide: MteRowComponent,
      useExisting: MteRowDefaultComponent,
    }
  ]
})
export class MteRowDefaultComponent extends MteRowComponent {}
