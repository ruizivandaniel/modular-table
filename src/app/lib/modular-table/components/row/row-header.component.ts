import { Component } from '@angular/core';
import { MteRowComponent } from './row.component';

@Component({
  selector: 'mte-row-header',
  template: `
    <ng-template #template>
      <div class="position-relative overflow-hidden w-100 h-100 row m-0 p-0">
        <ng-container *ngFor="let cell of cells">
          <div class="col p-0 m-0">
            <ng-template [ngTemplateOutlet]="cell.template"></ng-template>
          </div>
        </ng-container>
      </div>
    </ng-template>
  `,
  providers: [
    {
      provide: MteRowComponent,
      useExisting: MteRowHeaderComponent,
    }
  ]
})
export class MteRowHeaderComponent extends MteRowComponent { }
